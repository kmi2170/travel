export type CoordsType = { lat: number; lng: number };

export type BoundsType = {
  ne: { lat: number; lng: number };
  sw: { lat: number; lng: number };
};
