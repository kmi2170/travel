export const config = {
  title: 'Travel Companion',
  author: 'Kmi, Web developer/programmer',
  // author: {
  //   name: 'Kmi',
  //   summary: 'Web developer/programmer',
  // },
  description:
    'Find spot: hotels, restaurants, attractions on Map. You can search anywhere either on Map, by location names, or by addresses. More Detailed information is  shown in List.',
  keywords:
    'Fun Spot, Travel Advisor, Hotels, Restaurants, Attractions, Map, API,  React.js, Next.js, Material-UI, Leaflet Map',
  // social: {
  //   twitter: '',
  // },
};
